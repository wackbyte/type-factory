use {
	crate::{split, with, Unique},
	core::marker::PhantomData,
};

#[test]
fn macro_can_split() {
	with(|initial| {
		split!(let (a, _b, _c) = initial);
		split!(let (a,) = a);

		let (d, _e, _f) = split!((d, e, f) = a);
		let (_d,) = split!((d,) = d);
	});
}

#[test]
fn generics_without_lifetimes() {
	struct Branded<B>(PhantomData<fn(B) -> B>)
	where
		B: Unique;

	impl<B> Branded<B>
	where
		B: Unique,
	{
		fn new(_: &B) -> Self {
			Self(PhantomData)
		}
	}

	with(|initial| {
		let _branded = Branded::new(&initial);
		let (a, _) = split(initial);
		let _branded = Branded::new(&a);
	});
}

#[test]
fn types_impl() {
	use core::panic::{RefUnwindSafe, UnwindSafe};

	fn assert<T>(_: &T)
	where
		T: Send + Sync + Unpin + UnwindSafe + RefUnwindSafe,
	{
	}

	with(|initial| {
		assert(&initial);
		let (a, _) = split(initial);
		assert(&a);
	});
}

/// ```compile_fail,E0308
/// fn assert_type_eq<T>(_: T, _: T) {}
///
/// type_factory::with(|initial| {
///     let (a, b) = type_factory::split(initial);
///     assert_type_eq(a, b);
/// });
/// ```
fn _types_are_unique() {}

/// ```compile_fail,E0277
/// fn assert<T>(_: &T)
/// where
///     T: Copy + Clone,
/// {
/// }
///
/// type_factory::with(|initial| {
///     assert(&initial);
///     let (a, _) = type_factory::split(initial);
///     assert(&a);
/// });
/// ```
fn _types_not_impl() {}
