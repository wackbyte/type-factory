//! Generates unique values of opaque types.
//!
//! # Purpose
//!
//! <code>impl [Unique]</code> types are guaranteed to be unique. They cannot be
//! copied or cloned and are suitable for use as brand types.
//!
//! The [`toast-cell`](https://crates.io/crates/toast-cell) crate utilizes this
//! property.
//!
//! # Minimum supported Rust version
//!
//! The MSRV is currently 1.56.
//!
//! This may change between minor releases.
//!
//! # License
//!
//! I release this crate into the public domain using the
//! [Unlicense](https://unlicense.org/).
//!
//! # Similar crates
//!
//! [`generativity`](https://crates.io/crates/generativity) provides a macro to
//! generate unique invariant lifetimes. It does not require dependent code to
//! reside within a closure.

// Attributes
#![cfg_attr(not(any(doc, test)), no_std)]
// Lints
#![warn(missing_docs)]

use core::{
	fmt::{self, Debug, Formatter},
	marker::PhantomData,
};

#[cfg(any(doctest, test))]
mod tests;

mod sealed {
	pub trait Sealed {}
}
use sealed::Sealed;

/// The initial unique type.
///
/// This type can only be obtained using [`with`].
pub struct Initial<'scope>(PhantomData<fn(&'scope ()) -> &'scope ()>);

impl<'scope> Sealed for Initial<'scope> {}

impl<'scope> Debug for Initial<'scope> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str("impl Unique")
	}
}

/// A unique type.
///
/// The [`Initial`] unique type for a scope may be constructed using [`with`].
///
/// Further unique types may be obtained via [`split()`] or [`split!`].
///
/// # Invariant
///
/// `Unique` has one invariant: more than one instance of an `impl Unique` type
/// must *not* exist within a scope.
///
/// The crate is designed in such a way that this may never be violated in safe
/// code.
///
/// # Guarantees
///
/// As `impl Unique` types are opaque, they have a few properties that are not
/// possible to expose.
///
/// ## Types are zero-sized
///
/// All `impl Unique` types are zero-sized.
///
/// ## Types do nothing on drop
///
/// All `impl Unique` types can be [forgotten] without worry as they lack an
/// implementation of [`Drop`].
///
/// [forgotten]: std::mem::forget
///
/// # Safeguards
///
/// A number of safeguards are in place to prevent `Unique` from potential
/// misuse. The following is a list of these including examples of what could go
/// wrong if they were *not* in place.
///
/// ## The trait is not object-safe
///
/// `dyn Unique` trait objects are forbidden from existence for a reason. See
/// the following example:
///
/// ```compile_fail,E0038
/// # use type_factory::{split, with, Unique};
/// fn assert_type_eq<T>(_: T, _: T) {}
///
/// with(|initial| {
///     let (a, b) = split(initial);
///     let a: Box<dyn Unique> = Box::new(a);
///     let b: Box<dyn Unique> = Box::new(b);
///     // In this hypothetical world, unique types aren't so unique.
///     assert_type_eq(a, b);
/// });
/// ```
///
/// If `Unique` were object-safe, trait objects of it could be arbitrarily
/// created. As they would implement `Unique` while being the same type, the
/// trait's invariant would be violated.
///
/// ## The trait is sealed
///
/// `Unique` cannot be implemented for external types because it is [sealed].
/// See the following example:
///
/// ```compile_fail,E0277
/// # use type_factory::Unique;
/// fn assert_type_eq<T>(_: T, _: T) {}
///
/// #[derive(Debug)]
/// struct MyUnique;
///
/// impl Unique for MyUnique {}
///
/// let a = MyUnique;
/// let b = MyUnique;
/// // The invariant is trivially violated once again.
/// assert_type_eq(a, b);
/// ```
///
/// If the trait were not sealed, its invariant could easily be violated.
///
/// [sealed]: https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed
///
/// ## Values are tied to a scoped invariant lifetime
///
/// [`with`] and [`split()`] both have very particular function signatures:
///
/// ```
/// # use type_factory::{Initial, Unique};
/// fn with<R, F>(scope: F) -> R
/// where
///     F: for<'scope> FnOnce(Initial<'scope>) -> R,
/// # { type_factory::with(scope) }
/// { /* ... */ }
///
/// fn split<'scope, T>(unique: T) -> (impl 'scope + Unique, impl 'scope + Unique)
/// where
///     T: 'scope + Unique,
/// # { type_factory::split(unique) }
/// { /* ... */ }
/// ```
///
/// `'scope` can be thought of as the "scope" of the closure passed to [`with`].
/// No type bound by it can escape (or be returned from) the closure.
///
/// ```compile_fail
/// # use type_factory::{split, with};
/// with(|initial| {
///     // error: lifetime may not live long enough
///     split(initial)
/// });
/// ```
///
/// Furthermore, the lifetime is [invariant][variance] in order to guarantee
/// that `impl Unique` types are truly unique. If the lifetime were absent, such
/// types would be `'static`, allowing them to be [downcast] between each other
/// since they would all share the same [`TypeId`]. Consider the following
/// example:
///
/// ```
/// # use std::any::Any;
/// # use type_factory::Unique;
/// // Because all `impl Unique` values are backed by the same concrete type,
/// // they share a `TypeId`. This allows them to be downcast between each other
/// // thanks to `Box<dyn Any>`'s `downcast` methods.
/// fn unify<T, U>(x: T, y: U) -> Option<(T, T)>
/// where
///     T: 'static + Unique,
///     U: 'static + Unique,
/// {
///     let y: Box<dyn Any> = Box::new(y);
///     match y.downcast::<T>() {
///        Ok(y) => Some((x, *y)),
///        Err(_) => None,
///     }
/// }
/// ```
///
/// Thankfully, this is forbidden from occurring since, due to the design of the
/// crate, it is impossible to create an `impl Unique` type that is `'static`.
///
/// [variance]: https://doc.rust-lang.org/reference/subtyping.html
/// [downcast]: std::any
/// [`TypeId`]: std::any::TypeId
///
/// ## Values cannot be duplicated
///
/// This one is quite simple; no type that implements `Unique` also implements
/// [`Copy`] or [`Clone`] as to forbid its instance from being trivially
/// duplicated.
#[must_use = "unique types do nothing unless used"]
pub trait Unique: Sealed + Sized + Debug {}

impl<'scope> Unique for Initial<'scope> {}

/// Uses a unique type in a scope.
///
/// # Example
///
/// ```
/// # use type_factory::{split, with};
/// with(|initial/*: Initial<'_> */| {
///     let (a, b) = split(initial);
///     let (b, c) = split(b);
///     // `a`, `b`, and `c` are each of a distinct `impl '_ + Unique` type.
/// });
/// ```
pub fn with<R, F>(scope: F) -> R
where
	F: for<'scope> FnOnce(Initial<'scope>) -> R,
{
	scope(Initial(PhantomData))
}

/// Splits a unique type in two.
///
/// See also [`split!`].
///
/// # Example
///
/// ```
/// # use type_factory::{split, with};
/// with(|initial| {
///     // Split in two.
///     let (a, b) = split(initial);
///
///     // ...
/// });
/// ```
pub fn split<'scope, T>(
	#[allow(unused_variables)] unique: T,
) -> (impl 'scope + Unique, impl 'scope + Unique)
where
	T: 'scope + Unique,
{
	(Initial(PhantomData), Initial(PhantomData))
}

/// Splits a unique type into many.
///
/// See also [`split()`].
///
/// # Example
///
/// ```
/// # use type_factory::{split, with};
/// with(|initial| {
///     // Split into a tuple.
///     let (a, b, c, d, e, f) = split!((a, b, c, d, e, f) = initial);
///
///     // Destructure the tuple inline.
///     split!(let (g, h, i, j, k, l) = f);
///
///     // ...
/// })
/// ```
#[macro_export]
macro_rules! split {
	(($head:ident,) = $unique:expr $(;)?) => {
		{
			let ($head, _) = $crate::split($unique);
			($head,)
		}
	};
	(($head:ident $(, $tail:ident)+ $(,)?) = $unique:expr $(;)?) => {
		{
			#[allow(unused_variables)]
			let ($head, next) = $crate::split($unique);
			$(
				#[allow(unused_variables)]
				let ($tail, next) = $crate::split(next);
			)*
			($head, $($tail),*)
		}
	};
	(let ($head:ident,) = $unique:expr $(;)?) => {
		let ($head,) = $crate::split!(($head,) = $unique);
	};
	(let ($head:ident $(, $tail:ident)+ $(,)?) = $unique:expr $(;)?) => {
		let ($head, $($tail),*) = $crate::split!(($head, $($tail),*) = $unique);
	};
}
